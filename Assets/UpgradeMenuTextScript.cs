﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenuTextScript : MonoBehaviour {

    [SerializeField]
    private Text title;

    [SerializeField]
    private Text healthText;

    [SerializeField]
    private Text acText;

    [SerializeField]
    private Text shootText;

    [SerializeField]
    private Text stepText;

    [SerializeField]
    private Text ammoText;

    private PointScript point;
    private Stats stats;
    private TurnPiece turnPiece;
    private PlayerAttackScript attack;

    // Use this for initialization
    void Start () {

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        point = player.GetComponent<PointScript>();
        stats = player.GetComponent<Stats>();
        turnPiece = player.GetComponent<TurnPiece>();
        attack = player.GetComponent<PlayerAttackScript>();

	}
	
	// Update is called once per frame
	void Update () {

        title.text = "UPGRADE - " + point.getPoints() + "p";

        healthText.text = stats.Health.ToString();
        acText.text = stats.AC.ToString();
        shootText.text = stats.Shoot.ToString();

        stepText.text = turnPiece.maxSteps.ToString();
        ammoText.text = attack.maxAmmo.ToString();
		
	}
}
