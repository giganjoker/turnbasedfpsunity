﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveStatsScript : MonoBehaviour {

    public bool load = true;

    private PlayerAttackScript playerAttackScript;
    private Stats stats;
    private TurnPiece turnPiece;
    private PointScript point;

    public int defaultHealth;
    public int defaultAc;
    public int defaultShoot;
    public int defaultMaxAmmo;
    public int defaultMaxSteps;

    private void Start()
    {

        playerAttackScript = GetComponent<PlayerAttackScript>();
        stats = GetComponent<Stats>();
        turnPiece = GetComponent<TurnPiece>();
        point = GetComponent<PointScript>();

        if (load)
        {
            stats.Health = PlayerPrefs.GetInt("health", defaultHealth);
            stats.AC = PlayerPrefs.GetInt("ac", defaultAc);
            stats.Shoot = PlayerPrefs.GetInt("shoot", defaultShoot);
            playerAttackScript.maxAmmo = PlayerPrefs.GetInt("maxammo", defaultMaxAmmo);
            turnPiece.maxSteps = PlayerPrefs.GetInt("maxsteps", defaultMaxSteps);

            point.givePoint(PlayerPrefs.GetInt("points", 0));
        }

    }

    public void save()
    {
        PlayerPrefs.SetInt("health", stats.Health);
        PlayerPrefs.SetInt("ac", stats.AC);
        PlayerPrefs.SetInt("shoot", stats.Shoot);
        PlayerPrefs.SetInt("maxammo", playerAttackScript.maxAmmo);
        PlayerPrefs.SetInt("maxsteps", turnPiece.maxSteps);
        PlayerPrefs.SetInt("points", point.getPoints());
        PlayerPrefs.Save();
    }


}
