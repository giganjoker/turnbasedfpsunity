﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIScript : MonoBehaviour {


    private TileSetter tileSetter;
    private TurnPiece turnPeice;

    [SerializeField]
    private int attackStepCost = 2;
    [SerializeField]
    private int moveStepCost = 1;

    TileSetter playerTileSetter;

    TileManagerScript tileManager;

    [SerializeField]
    private int damage;

    public int attackRange;

    [SerializeField]
    private RngManager.Dice attackDice;

    private Stats myStats;

    private Stats playerStats;

    [SerializeField]
    private LayerMask whatIsSolid;

    [SerializeField]
    private LayerMask coverLayerMask;

    [SerializeField]
    private float animateMoveHeight = 5.0f;

    private float originalY;

    //[SerializeField]
    //private int retreatRange;

    // Use this for initialization
    void Start () {

        turnPeice = GetComponent<TurnPiece>();
        tileSetter = GetComponent<TileSetter>();
        myStats = GetComponent<Stats>();

        playerTileSetter = GameObject.FindGameObjectWithTag("Player").GetComponent<TileSetter>();
        tileManager = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<TileManagerScript>();
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<Stats>();
    }

    private bool moving = false;
    private float journeyFrac = 0;

    void moveLoop()
    {
        transform.position = Vector3.Lerp(tileSetter.prevTile.basePos, tileSetter.onTile.basePos, journeyFrac);

        float yJourneyFrac = journeyFrac * 2;

        if (yJourneyFrac > 1)
            yJourneyFrac -= 2;

        yJourneyFrac = Mathf.Abs(yJourneyFrac);

        float y = Mathf.SmoothStep(originalY, originalY + animateMoveHeight, yJourneyFrac);

        transform.position = new Vector3(transform.position.x, y, transform.position.z);

        journeyFrac += Time.deltaTime / 1.0f;


        if (journeyFrac >= 1.0f)
            moving = false;
    }

    // Update is called once per frame
    void Update () {

        if (moving)
        {
            moveLoop();
            return;
        }

        if (!turnPeice.isMyTurn)
            return;

        //check if in attackrange of enemy
        TileScript[] adjTiles = tileManager.getAdjecentTiles(tileSetter.onTile.cood).ToArray();
        Vector2 playerDist = playerTileSetter.onTile.cood - tileSetter.onTile.cood;

        bool isInAttackRange = Vector3.Distance(transform.position, playerTileSetter.transform.position) <= attackRange;
        bool hasValidMove = true;
        //true attack
        if (isInAttackRange)
            attack();
        //false move in the direction of the player
        else
        {

            List<TileScript> validTiles = new List<TileScript>();

            foreach (TileScript tile in adjTiles)
            {
                Vector2 tileDir = tile.cood - tileSetter.onTile.cood;

                if (
                    ((Mathf.Sign(tileDir.x) == Mathf.Sign(playerDist.x) && tile.canMoveTo(gameObject, tileSetter.onTile)) ||
                    (Mathf.Sign(tileDir.y) == Mathf.Sign(playerDist.y) && tile.canMoveTo(gameObject, tileSetter.onTile))) &&
                    tile != tileSetter.prevTile
                    )
                    validTiles.Add(tile);
            }

            if (validTiles.Count > 0)
            {
                int tileIndex = Random.Range(0, validTiles.Count);
                move(validTiles[tileIndex]);

            }
            else
                hasValidMove = false;

        }

        if (
            (isInAttackRange && !turnPeice.haveSteps(attackStepCost)) ||
            (!isInAttackRange && (!turnPeice.haveSteps(moveStepCost) || !hasValidMove)) &&
            !moving
            )
        {
            Vector3 dir = (playerStats.transform.position - transform.position).normalized;
            float angle = Mathf.Atan2(dir.x, dir.z);
            transform.rotation = Quaternion.Euler(0, angle * Mathf.Rad2Deg, 0);

            turnPeice.turnComplete();
        }

	}

    void attack()
    {
        if (!turnPeice.conumeStep(attackStepCost))
            return;

        RaycastHit hit;

        if (Physics.Linecast(transform.position, playerStats.transform.position, out hit, whatIsSolid))
        {

            if(hit.collider.tag == "Mountain")
            {
                print("saved by mountain");
                GameObject.FindGameObjectWithTag("ui").GetComponent<IngameMenuScript>().warn("Enemy shot but saved by mountain.",0.5f);
                return;
            }

            if (hit.collider.tag == "Player")
            {

                Vector3 dir = (playerStats.transform.position - transform.position).normalized;
                RaycastHit[] coverHitChecks = Physics.RaycastAll(transform.position, dir, attackRange, coverLayerMask);

                int shootDebuff = 0;
                int playerACBuff = 0;
                foreach(RaycastHit coverHitCheck in coverHitChecks)
                {
                    if (coverHitCheck.collider.tag == "cover")
                    {
                        GameObject tile = coverHitCheck.collider.transform.parent.gameObject;


                        if (tileSetter.onTile.cood == tile.GetComponent<TileScript>().cood)
                            shootDebuff = tile.GetComponent<CoverScript>().directionalShootDebuff;
                        if (playerTileSetter.onTile.cood == tile.GetComponent<TileScript>().cood)
                            playerACBuff = tile.GetComponent<CoverScript>().directionalACBuff;
                    }
                }


                if (shootDebuff != 0 || playerACBuff != 0)
                {
                    print("shootDebuff:" + shootDebuff);
                    print("playerACBuff:" + playerACBuff);
                }

                print("enemy: attack");
                if (RngManager.RollDice(attackDice) + myStats.Shoot - shootDebuff >= playerStats.AC + playerACBuff)
                {
                    playerStats.hurt(damage);
                    GameObject.FindGameObjectWithTag("ui").GetComponent<IngameMenuScript>().warn("You took Damage.");

                    print("player ouch");
                }
            }
        }
    }

    void move(TileScript tile)
    {
        if (!turnPeice.conumeStep(moveStepCost))
            return;

        originalY = transform.position.y;

        tileSetter.moveTo(tile,1);

        moving = true;
        journeyFrac = 0;

    }

}
