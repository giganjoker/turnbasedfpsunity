﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnPiece : MonoBehaviour {

    protected TurnManager turnManager;

    public int maxSteps = 3;
    public int steps;

    //use to code in update instead of play
    public bool isMyTurn;


	// Use this for initialization
	public void Awake () {

        turnManager = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<TurnManager>();

        turnManager.addPiece(this);

	}

    public void remove()
    {
        turnManager.removePiece(this);
    }

    public void playTurn()
    {
        steps = maxSteps;
        isMyTurn = true;
    }

    public void turnComplete()
    {
        isMyTurn = false;
        turnManager.turnComplete();
    }

    public bool haveSteps(int cost)
    {
        

        return cost <= steps;
    }

    public bool conumeStep(int cost)
    {
        if (!haveSteps(cost))
            return false;

        steps -= cost;

        return true;
    }

    public bool canDoAnything(params int[] costs)
    {

        foreach(int cost in costs)
        {
            if (steps >= cost)
                return true;
        }

        return false;

    }

}
