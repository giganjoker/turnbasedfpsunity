﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour {

    public Vector3 basePos;
    public Vector2 cood;
    
    public float baseOffsetY = 1.9f;

    public GameObject onMe;

    public CoverScript cover;
    private MountainScript mountain;
    private RiverTileScript riverTileScript;
    private HoleTileScript holeTile;

    // Use this for initialization
    void Awake () {
        basePos = new Vector3(transform.position.x, transform.position.y + baseOffsetY, transform.position.z);
        cover = GetComponent<CoverScript>();
        mountain = GetComponent<MountainScript>();
        riverTileScript = GetComponent<RiverTileScript>();
        holeTile = GetComponent<HoleTileScript>();
    }

    public bool canMoveTo(GameObject peice, TileScript fromTile)
    {
        if (onMe != null)
            return false;

        if (mountain != null)
            return false;

        if (riverTileScript != null)
            return false;

        if (fromTile != null)
        {
            if (!fromTile.isLeavable(peice, this))
                return false;

            if (cover != null)
            {
                Vector2 coodDiff = fromTile.cood - cood;
                int side = getSideFromCoodDif(coodDiff);

                foreach (int s in cover.sides)
                {
                    if (s == side)
                        return false;
                }
            }
        }

        return true;
    }

    public bool isLeavable(GameObject peice, TileScript toTile)
    {

        if(cover !=  null)
        {
            Vector2 coodDiff = toTile.cood - cood;
            int side = getSideFromCoodDif(coodDiff);
            foreach(int s in cover.sides)
            {
                if (s == side)
                    return false;
            }
        }

        return true;

    }

    private static int getSideFromCoodDif(Vector2 coodDiff)
    {
        if (coodDiff.x == 0 && coodDiff.y == 1)
            return 0;
        else if (coodDiff.x == 1 && coodDiff.y == 0.5)
            return 1;
        else if (coodDiff.x == 1 && coodDiff.y == -0.5)
            return 2;
        else if (coodDiff.x == 0 && coodDiff.y == -1)
            return 3;
        else if (coodDiff.x == -1 && coodDiff.y == -0.5)
            return 4;
        else if (coodDiff.x == -1 && coodDiff.y == 0.5)
            return 5;
        else return -1;


    }

    public void sitOn(GameObject piece, float animationTime)
    {
        onMe = piece;
        StartCoroutine(waitForAnimation(piece, animationTime));
    }

    public void moveAway()
    {
        onMe = null;
    }

    IEnumerator waitForAnimation(GameObject piece, float time)
    {
        yield return new WaitForSeconds(time);
        piece.transform.position = basePos;

        if(holeTile!=null)
            piece.GetComponent<Stats>().die();
    }
}
