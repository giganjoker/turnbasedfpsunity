﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackScript : MonoBehaviour {

    [SerializeField]
    private int damage;

    [SerializeField]
    private int spawnerDamage;

    public int attackRange;

    [SerializeField]
    private int reloadCost;

    public int maxAmmo;

    public int ammo;

    private TurnPiece turnPiece;

    [SerializeField]
    private RngManager.Dice attackDice;

    [SerializeField]
    private LayerMask whatIsSolid;

    [SerializeField]
    private LayerMask coverLayerMask;

    private TileSetter tileSetter;
    private IngameMenuScript ui;

    private void Awake()
    {
        turnPiece = GetComponent<TurnPiece>();
        tileSetter = GetComponent<TileSetter>();
        ammo = maxAmmo;

        ui = GameObject.FindGameObjectWithTag("ui").GetComponent<IngameMenuScript>();
    }

    // Update is called once per frame
    void Update () {

        if (!turnPiece.isMyTurn)
            return;

        if (Input.GetKeyUp("f"))
            shoot();

        if (Input.GetKeyUp("r"))
            reload();

    }

    void shoot()
    {
        if (ammo <= 0)
        {
            print("mag empty");
            ui.warn("No ammo, press R to reload.", 0.5f);
            return;
        }

        Transform gun = transform.Find("barrel");

        float angle = gun.rotation.eulerAngles.y * -1;
        Vector3 pos = gun.position;

        Vector3 dir = Vector3.forward * Mathf.Cos(angle * Mathf.Deg2Rad) + Vector3.left * Mathf.Sin(angle * Mathf.Deg2Rad);
        RaycastHit hit;


        print("tryint to shoot");

        if (Physics.Raycast(pos, dir, out hit, attackRange, whatIsSolid))
        {
            ammo--;

            print("pew");

            print(hit.collider.tag);

            if (hit.collider.tag == "Mountain")
            {
                ui.warn("Enemy saved by mountain.", 0.5f);
                print("blocked by mountain");
                return;
            }

            if (hit.collider.tag == "Enemy")
            {

                int shoot = GetComponent<Stats>().Shoot;
                Stats enemyStats = hit.collider.gameObject.GetComponent<Stats>();

                RaycastHit[] coverHitChecks = Physics.RaycastAll(pos, dir, attackRange, coverLayerMask);

                int shootDebuff = 0;
                int enemyACBuff = 0;
                foreach(RaycastHit coverHitCheck in coverHitChecks)
                {
                    if (coverHitCheck.collider.tag == "cover")
                    {
                        GameObject tile = coverHitCheck.collider.transform.parent.gameObject;


                        if (tileSetter.onTile.cood == tile.GetComponent<TileScript>().cood)
                            shootDebuff = tile.GetComponent<CoverScript>().directionalShootDebuff;
                        if (enemyStats.gameObject.GetComponent<TileSetter>().onTile.cood == tile.GetComponent<TileScript>().cood)
                            enemyACBuff = tile.GetComponent<CoverScript>().directionalACBuff;
                    }
                }

                if(shootDebuff!=0 || enemyACBuff != 0)
                {
                    print("shootDebuff:" + shootDebuff);
                    print("enemyAcbuff:" + enemyACBuff);
                }
                if (RngManager.RollDice(attackDice) + shoot >= enemyStats.AC + enemyACBuff)
                {
                    enemyStats.hurt(damage);
                    print("Enemy took damage.");
                    ui.warn("Enemy took damage.",0.5f);
                }
                else
                {
                    ui.warn("Shot missed.", 0.5f);
                    print("missed");
                }

            }

            if(hit.collider.tag == "Spawner")
            {
                print("damaged spawner");
                ui.warn("Spawner took damage.",0.5f);
                hit.collider.GetComponent<SpawnerHealth>().hurt(spawnerDamage);
            }

        }
        else
        {
            ui.warn("Enemy is out of range");
        }
    }

    void reload()
    {
        if (!turnPiece.conumeStep(reloadCost))
            return;

        ammo = maxAmmo;
        print("reloaded");
    }

}
