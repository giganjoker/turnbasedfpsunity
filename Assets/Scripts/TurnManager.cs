﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour {

    private List<TurnPiece> pieces = new List<TurnPiece>();
    private int playingPiece;
    [HideInInspector]
    public bool isPlaying;

    public void turnComplete()
    {
        isPlaying = false;
    }

    public void addPiece(TurnPiece piece)
    {
        pieces.Add(piece);
    }

    public void removePiece(TurnPiece piece)
    {
        pieces.Remove(piece);
    }

    private void Start()
    {
        int i;
        TurnPiece hero = null;
        for (i=0; i<pieces.Count; i++)
        {
            if(pieces[i].gameObject.tag == "Player")
            {
                hero = pieces[i];
                break;
            }
        }

        if (hero != null)
        {
            pieces.Remove(hero);
            pieces.Insert(0, hero);
        }

    }

    // Update is called once per frame
    void Update () {

        if (!isPlaying)
        {
            if (playingPiece >= pieces.Count)
                playingPiece = 0;

            isPlaying = true;
            pieces[playingPiece].playTurn();

            playingPiece++;
        }

    }
}
