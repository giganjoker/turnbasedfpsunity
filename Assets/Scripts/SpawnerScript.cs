﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour {

    
    [SerializeField]
    private GameObject[] enemies;
    private TileScript[] adjacentTiles;

    private TileSetter tileSetter;

    public int spawnStepCost = 3;

    private TurnPiece turnPiece;

    [SerializeField]
    private int SpawnAC;

    [SerializeField]
    private RngManager.Dice diceType;

	// Use this for initialization
	void Start () {
        tileSetter = GetComponent<TileSetter>();
        turnPiece = GetComponent<TurnPiece>();
        adjacentTiles = tileSetter.tileManager.getAdjecentTiles(tileSetter.startOnTile).ToArray();
	}
	
    public void Spawn()
    {

        GameObject enemyPrefab = enemies[Random.Range(0, enemies.Length)];
        TileScript adjTile = null;

        //find a free tile
        foreach(TileScript tile in adjacentTiles)
        {
            if(tile.canMoveTo(enemyPrefab, null))
            {
                adjTile = tile;
                break;
            }
        }

        //if free tile not found skip turn
        if(adjTile == null)
        {
            turnPiece.turnComplete();
            return;
        }

        if (!turnPiece.conumeStep(spawnStepCost))
            return;

        //chance to spawn
        if (RngManager.RollDice(diceType) < SpawnAC)
            return;

        GameObject enemy = Instantiate(
                enemyPrefab,
                adjTile.basePos,
                adjTile.transform.rotation,
                transform
            );
        enemy.GetComponent<TileSetter>().startOnTile = adjTile.cood;
    }

    private void Update()
    {
        if (!turnPiece.isMyTurn)
            return;

        Spawn();

        if (turnPiece.steps <= 0)
            turnPiece.turnComplete();
    }

}
