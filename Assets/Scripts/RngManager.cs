﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RngManager : MonoBehaviour {

	
    public static int RollDice(Dice dice)
    {
        return Random.Range(0, dice.noOfDice * dice.noOfSides);
    }

    [System.Serializable]
    public struct Dice
    {
        public int noOfDice;
        public int noOfSides;
    }

}
