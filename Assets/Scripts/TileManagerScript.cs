﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManagerScript : MonoBehaviour {

    
    List<TileScript> tiles = new List<TileScript>();

	// Use this for initialization
	void Awake () {

        GameObject[] tileGOs = GameObject.FindGameObjectsWithTag("Tile");

        foreach(GameObject go in tileGOs){
            tiles.Add(go.GetComponent<TileScript>());
        }

	}

    public TileScript getTile(Vector2 cood)
    {
        foreach(TileScript t in tiles)
        {
            if (t.cood == cood)
                return t;
        }
        return null;
    } 

    public List<TileScript> getAdjecentTiles(Vector2 cood)
    {
        List<TileScript> tiles = new List<TileScript>();

        addIfNotNull(ref tiles,getTile(new Vector2(cood.x, cood.y + 1)));
        addIfNotNull(ref tiles, getTile(new Vector2(cood.x, cood.y - 1)));

        addIfNotNull(ref tiles, getTile(new Vector2(cood.x + 1, cood.y + 0.5f)));
        addIfNotNull(ref tiles, getTile(new Vector2(cood.x + 1, cood.y - 0.5f)));

        addIfNotNull(ref tiles, getTile(new Vector2(cood.x - 1, cood.y + 0.5f)));
        addIfNotNull(ref tiles, getTile(new Vector2(cood.x - 1, cood.y - 0.5f)));

        return tiles;
    }

    public TileScript getAdjecentTile(Vector2 cood, int neighborNo)
    {
        switch (neighborNo)
        {
            case 0:
                return getTile(new Vector2(cood.x, cood.y + 1));
            case 1:
                return getTile(new Vector2(cood.x + 1, cood.y + 0.5f));
            case 2:
                return getTile(new Vector2(cood.x + 1, cood.y - 0.5f));
            case 3:
                return getTile(new Vector2(cood.x, cood.y - 1));
            case 4:
                return getTile(new Vector2(cood.x - 1, cood.y - 0.5f));
            case 5:
                return getTile(new Vector2(cood.x - 1, cood.y + 0.5f));
            default:
                return null;
        }
    }

    public void addIfNotNull(ref List<TileScript> tiles, TileScript tile)
    {
        if (tile != null)
            tiles.Add(tile);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
