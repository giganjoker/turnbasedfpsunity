﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveScript : MonoBehaviour {


    private float playerOffsetY;

    private Transform player;

	// Use this for initialization
	void Awake () {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        playerOffsetY = transform.position.y - player.position.y;

    }
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(player.position.x, player.position.y + playerOffsetY, player.position.z);
	}
}
