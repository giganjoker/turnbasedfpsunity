﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Stats : MonoBehaviour {

    public int Health = 5;
    public int AC = 5;
    public int Shoot = 1;

    public void hurt(int damage)
    {
        Health -= damage;

        if (Health <= 0)
            die();
    }

    public abstract void die();

}
