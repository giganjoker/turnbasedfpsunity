﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSetter : MonoBehaviour {

    public Vector2 startOnTile;

    [HideInInspector]
    public TileScript onTile;
    [HideInInspector]
    public TileScript prevTile;

    [HideInInspector]
    public TileManagerScript tileManager;

    private IngameMenuScript ui;


    // Use this for initialization
    void Start () {
        tileManager = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<TileManagerScript>();
        onTile = tileManager.getTile(startOnTile);
        onTile.sitOn(gameObject, 0);

        ui = GameObject.FindGameObjectWithTag("ui").GetComponent<IngameMenuScript>();
    }
	
    public bool moveTo(TileScript tile, float animationTime)
    {
        if (!tile.canMoveTo(gameObject, onTile))
            return false;

        onTile.moveAway();

        prevTile = onTile;
        onTile = tile;
        tile.sitOn(gameObject, animationTime);

        if (onTile.cover != null)
            ui.setCoverImage(onTile.cover.coverType);
        else
            ui.setCoverImage(CoverScript.Type.NoCover);


        return true;
    }

}
