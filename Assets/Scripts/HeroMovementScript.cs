﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroMovementScript : MonoBehaviour {

    private Transform myCamera;

    private TileManagerScript tileManagerScript;

    private bool moving = false;

    [SerializeField]
    private float timeToMoveOneTile;


    [SerializeField]
    private Vector2[] angleRange;

    private TurnPiece turnPiece;

    [SerializeField]
    private int MoveStepCost = 1;

    private IngameMenuScript ui;
    private TileSetter tileSetter;

    // Use this for initialization
    void Start () {
        myCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;

        tileManagerScript = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<TileManagerScript>();
        ui = GameObject.FindGameObjectWithTag("ui").GetComponent<IngameMenuScript>();

        turnPiece = GetComponent<TurnPiece>();
        tileSetter = GetComponent<TileSetter>();
	}


    private float journeyFrac;
    /**
     * Called once to move to target tile
     */
    void move(TileScript toTile)
    {

        if (!toTile.canMoveTo(gameObject, tileSetter.onTile))
            return;


        if (!turnPiece.conumeStep(MoveStepCost))
        {
            ui.warn("no more steps, press X for finish turn.");
            return;
        }
        
        if (!tileSetter.moveTo(toTile, timeToMoveOneTile))
            return;

        moving = true;
        journeyFrac = 0;
    }


    //override
    void move(Vector2 toTileCood)
    {
        move(tileManagerScript.getTile(toTileCood));
    }

    void moveLoop()
    {
        transform.position = Vector3.Lerp(tileSetter.prevTile.basePos, tileSetter.onTile.basePos, journeyFrac);
        journeyFrac += Time.deltaTime / timeToMoveOneTile;


        if (journeyFrac >= 1.0f)
            moving = false;
    }
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, myCamera.eulerAngles.y, transform.eulerAngles.z);

        if (moving)
            moveLoop();

        if (!turnPiece.isMyTurn)
            return;

        //moving initiating script
        if(Input.GetAxisRaw("Vertical") == 1 && !moving)
        {
            float angle = transform.eulerAngles.y;

            for (int i = 1; i<angleRange.Length; i++)
            {
                if(angle > angleRange[i].x && angle < angleRange[i].y)
                {
                    TileScript t = tileManagerScript.getAdjecentTile(tileSetter.onTile.cood, i);
   
                    if(t!=null)
                        move(t);
                    break;
                }

            }
            if (angle > angleRange[0].x || angle < angleRange[0].y)
            {
                TileScript t = tileManagerScript.getAdjecentTile(tileSetter.onTile.cood, 0);

                if (t != null)
                    move(t);
            }

        }

        if (Input.GetKeyUp("x"))
            turnPiece.turnComplete();

    }
}
