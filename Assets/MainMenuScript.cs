﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

    [SerializeField]
    private GameObject contolsDialog;

    public void play()
    {
        SceneManager.LoadScene(1);
    }

    public void showControls()
    {
        contolsDialog.SetActive(true);
    }

    public void hideControls()
    {
        contolsDialog.SetActive(false);
    }

    public void quit()
    {
        Application.Quit();
    }

}
