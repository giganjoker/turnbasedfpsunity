﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerAttackScript))]
public class PlayerAttackRange : Editor {

    public void OnSceneGUI()
    {
        PlayerAttackScript player = ((PlayerAttackScript)target).GetComponent<PlayerAttackScript>();

        Handles.color = Color.red;
        Handles.DrawWireArc(player.transform.position, Vector3.up, Vector3.right, 360, player.attackRange);
    }

}
