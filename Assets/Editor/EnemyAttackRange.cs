﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(EnemyAIScript))]
public class EnemyAttackRange : Editor {


    public void OnSceneGUI()
    {
        EnemyAIScript enemy = ((EnemyAIScript)target).GetComponent<EnemyAIScript>();

        Handles.color = Color.red;
        Handles.DrawWireArc(enemy.transform.position, Vector3.up, Vector3.right, 360, enemy.attackRange);
    }
	
}
