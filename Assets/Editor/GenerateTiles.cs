﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GenerateTiles : MonoBehaviour {
    

    [MenuItem("MyTools/GenerateTiles")]
	static void CreateTiles()
    {


        Vector2 tileDelta = new Vector2(9.22f,10.25f);
        Vector3 startPos = new Vector3(7.18f, -15.2f, 16.7f);
        Vector2 gridSize = new Vector2(10,10);
        GameObject tilePrefab = Resources.Load("prefabs/tile") as GameObject;



        GameObject parent = GameObject.FindGameObjectWithTag("TileParent");

        for (float i = 0; i < gridSize.x; i++)
        {
            for (float j = i % 2 == 0 ? 0.0f : 0.5f; j < gridSize.y; j++)
            {

                Vector3 pos = new Vector3(
                        startPos.x + i * tileDelta.x,
                        startPos.y,
                        startPos.z + j * tileDelta.y
                    );

                GameObject tileGO = Instantiate(
                    tilePrefab,
                    pos,
                    parent.transform.rotation,
                    parent.transform
                    );

                TileScript tile = tileGO.GetComponent<TileScript>();

                tile.cood = new Vector2(i, j);
                //tile.basePos = new Vector3(pos.x, pos.y + baseOffsetY, pos.z);


            }
        }


    }

    [MenuItem("MyTools/ClearTiles")]
    static void clearTiles()
    {
        GameObject[] buggedTiles = GameObject.FindGameObjectsWithTag("Tile");

        foreach(GameObject tile in buggedTiles)
        {
            if (tile.transform.childCount <= 0)
                DestroyImmediate(tile);
        }
    }
	
}
