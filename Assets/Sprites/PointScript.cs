﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointScript : MonoBehaviour {

    [SerializeField]
    private int points;

    private Stats stats;
    private IngameMenuScript ui;

    private void Start()
    {
        stats = GetComponent<Stats>();
        ui = GameObject.FindGameObjectWithTag("ui").GetComponent<IngameMenuScript>();
        ui.warn("You got Point(s) press TAB to Upgrade.", 1.5f);

    }

    public void givePoint(int p=1)
    {
        points+=p;

        if(ui != null)
            ui.warn("You got Point(s) press TAB to Upgrade.");
    }

    public int getPoints()
    {
        return points;
    }

    private void Update()
    {
        if (Input.GetKeyUp("1"))
        {
            upgrade(ref stats.Health);
            print("upgraded health");
        }
        else if(Input.GetKeyUp("2"))
        {
            upgrade(ref stats.AC);
            print("upgraded AC");
        }
        else if(Input.GetKeyUp("3"))
        {
            upgrade(ref stats.Shoot);
            print("upgraded Shoot");
        }
        
    }

    public void upgradeHealth()
    {
        upgrade(ref stats.Health);
    }

    public void upgradeAC()
    {
        upgrade(ref stats.AC);
    }

    public void upgradeShoot()
    {
        upgrade(ref stats.Shoot);
    }
    public void upgradeStep()
    {
        upgrade(ref GetComponent<TurnPiece>().maxSteps);
    }
    public void upgradeMag()
    {
        upgrade(ref GetComponent<PlayerAttackScript>().maxAmmo);
    }

    void upgrade(ref int stat)
    {
        if (points <= 0)
            return;

       points--;
       stat++;
    }
}
