﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverScript : MonoBehaviour {

    public int[] sides;

    public int directionalACBuff;
    public int directionalShootDebuff;

    public Type coverType;

    private float y = 1.5f;

    private Vector2[] pos =
    {
        new Vector2(0,5),
        new Vector2(4.18f, 2.15f),
        new Vector2(4.18f, -2.15f),
        new Vector2(0,-5),
        new Vector2(-4.18f, -2.15f),
        new Vector2(-4.18f, 2.15f)

    };

    private float[] yRot =
    {
        90,
        150,
        210,
        270,
        330,
        30
    };

	// Use this for initialization
	void Start () {
		
        foreach(int side in sides)
        {

            GameObject wallPrefab = Resources.Load("prefabs/cover") as GameObject; 
            GameObject wall = Instantiate(wallPrefab, transform);
            wall.transform.localPosition = new Vector3(pos[side].x, y, pos[side].y);

            wall.transform.localRotation = Quaternion.Euler(0, yRot[side], 0);
            
        }

	}
	
    [System.Serializable]
    public enum Type
    {
        Full,Half,NoCover
    }
}
