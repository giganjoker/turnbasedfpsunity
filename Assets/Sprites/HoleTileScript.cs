﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleTileScript : MonoBehaviour {

	// Use this for initialization
	void Start () {

        GameObject riverTile = Resources.Load("prefabs/holetile") as GameObject;
        Destroy(transform.Find("Cylinder001").gameObject);
        Instantiate(riverTile, transform);

    }
	
}
