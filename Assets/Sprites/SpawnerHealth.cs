﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerHealth : MonoBehaviour {

    public int maxHealth = 10;
    public int health;

    private void Start()
    {
        health = maxHealth;
    }

    public void hurt(int damage)
    {
        health -= damage;
        
        if(health <= 0)
        {
            die();
        }
    }

    public void die()
    {
        GameObject.FindGameObjectWithTag("GameMaster").GetComponent<ObjectiveManager>().removeSpawner(gameObject);
        GameObject.FindGameObjectWithTag("GameMaster").GetComponent<TurnManager>().removePiece(GetComponent<TurnPiece>());
        GameObject.FindGameObjectWithTag("Player").GetComponent<PointScript>().givePoint();
        
        Destroy(gameObject);
    }

}
