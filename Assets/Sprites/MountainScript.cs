﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainScript : MonoBehaviour {

	// Use this for initialization
	void Start () {

        GameObject moutainPrefab = Resources.Load("prefabs/mountain") as GameObject;
        Instantiate(moutainPrefab, transform);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
