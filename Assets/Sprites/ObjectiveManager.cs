﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveManager : MonoBehaviour {

    private List<GameObject> spawners = new List<GameObject>();

    public int nextLvlIndex;

    private void Start()
    {
        spawners = new List<GameObject>(GameObject.FindGameObjectsWithTag("Spawner"));
    }

    public void removeSpawner(GameObject s)
    {
        spawners.Remove(s);

        if(spawners.Count <= 0)
        {
            print("you win!");
            GameObject.FindGameObjectWithTag("ui").GetComponent<IngameMenuScript>().won();

        }
    }


}
