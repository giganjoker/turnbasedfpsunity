﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverTileScript : MonoBehaviour {
	
	// Update is called once per frame
	void Start () {

        GameObject riverTile = Resources.Load("prefabs/rivertile") as GameObject;
        Destroy(transform.Find("Cylinder001").gameObject);
        Instantiate(riverTile, transform);

    }


}
