﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IngameMenuScript : MonoBehaviour {


    private GameObject crossHairCanvas;

    private CameraRotateScript camera;

    [SerializeField]
    private GameObject pauseDialog;

    [SerializeField]
    private GameObject upgradeDialog;

    [SerializeField]
    private GameObject HUD;

    private bool pauseOpen = false;
    private bool upgradeOpen = false;
    private bool deadOpen = false;
    private bool wonOpen = false;

    [SerializeField]
    private Image bulletImage;

    [SerializeField]
    private Text currentAmmoLeft;

    [SerializeField]
    private Text currentStepsLeft;

    [SerializeField]
    private Text healthLeft;

    private PlayerAttackScript playerAttackScript;
    private TurnPiece playerTurnPiece;
    private Stats stats;

    [SerializeField]
    private Sprite onBullet;

    [SerializeField]
    private Sprite offBullet;

    [SerializeField]
    private Text warnText;

    [SerializeField]
    private GameObject warnTextBox;

    [SerializeField]
    private GameObject retryUi;

    [SerializeField]
    private GameObject wonDialog;

    [SerializeField]
    private GameObject enmiesTurnBox;

    private PointScript pointScript;

    private void Awake()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraRotateScript>();
        crossHairCanvas = GameObject.FindGameObjectWithTag("crosshair");

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerAttackScript = player.GetComponent<PlayerAttackScript>();
        playerTurnPiece = player.GetComponent<TurnPiece>();
        stats = player.GetComponent<Stats>();
        pointScript = player.GetComponent<PointScript>();

        Cursor.visible = false;
    }


    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (!pauseOpen)
                pause();
            else
                unPause();
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            if (!upgradeOpen)
                upgrade();
            else
                closeUpgrade();
        }

        //hud
        currentAmmoLeft.text = playerAttackScript.ammo.ToString();

        if (playerAttackScript.ammo > 0)
            bulletImage.sprite = onBullet;
        else
        {
            bulletImage.sprite = offBullet;
        }

        currentStepsLeft.text = playerTurnPiece.steps.ToString();

        healthLeft.text = stats.Health.ToString();


        if (playerTurnPiece.isMyTurn && enmiesTurnBox.activeSelf)
            enmiesTurnBox.SetActive(false);

        if (!playerTurnPiece.isMyTurn && !enmiesTurnBox.activeSelf)
            enmiesTurnBox.SetActive(true);

    }

    public void died()
    {
        deadOpen = true;
        retryUi.SetActive(true);
        holdStuff();
    }

    public void retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void won()
    {
        wonOpen = true;
        wonDialog.SetActive(true);
        holdStuff();
    }

    public void nextLvl()
    {
        int nextLvl = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<ObjectiveManager>().nextLvlIndex;

        GameObject.FindGameObjectWithTag("Player").GetComponent<SaveStatsScript>().save();

        if (nextLvl == -1)
            SceneManager.LoadScene(0);

        SceneManager.LoadScene(nextLvl);
    }

    public void warn(string text, float forSec = 0.75f)
    {
        warnText.text = text;
        warnTextBox.SetActive(true);
        StartCoroutine(resetWarn(forSec));
    }

    IEnumerator resetWarn(float time)
    {
        yield return new WaitForSeconds(time);
        warnTextBox.SetActive(false);
    }

    public void pause()
    {
        if (pauseOpen || upgradeOpen || deadOpen || wonOpen)
            return;

        holdStuff();
        pauseDialog.SetActive(true);
        pauseOpen = true;
    }

    public void unPause()
    {
        if (!pauseOpen)
            return;

        unholdStuff();
        pauseDialog.SetActive(false);
        pauseOpen = false;
    }

    public void upgrade()
    {
        if (pauseOpen || upgradeOpen || deadOpen || wonOpen)
            return;

        holdStuff();
        upgradeDialog.SetActive(true);
        upgradeOpen = true;
    }

    public void closeUpgrade()
    {
        if (!upgradeOpen)
            return;

        unholdStuff();
        upgradeDialog.SetActive(false);
        upgradeOpen = false;
    }


    public void quitToMain()
    {
        SceneManager.LoadScene(0);
    }

    private void holdStuff()
    {
        HUD.SetActive(false);
        camera.pauseRotate = true;
        crossHairCanvas.SetActive(false);
        Cursor.visible = true;
    }

    private void unholdStuff()
    {
        HUD.SetActive(true);
        camera.pauseRotate = false;
        crossHairCanvas.SetActive(true);
        Cursor.visible = false;

    }

    [SerializeField]
    private GameObject halfCoverImage;

    [SerializeField]
    private GameObject fullCoverImage;

    public void setCoverImage(CoverScript.Type type)
    {
        halfCoverImage.SetActive(false);
        fullCoverImage.SetActive(false);

        if (type == CoverScript.Type.Half)
            halfCoverImage.SetActive(true);
        else if (type == CoverScript.Type.Full)
            fullCoverImage.SetActive(true);
    }



    public void upgradeHealth()
    {
        pointScript.upgradeHealth();
    }

    public void upgradeAC()
    {
        pointScript.upgradeAC();
    }

    public void upgradeShoot()
    {
        pointScript.upgradeShoot();
    }
    public void upgradeStep()
    {
        pointScript.upgradeStep();
    }
    public void upgradeMag()
    {
        pointScript.upgradeMag();
    }



}
